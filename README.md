Core support for backend applications based on `CDI` and `microprofile-config`.

Expects a matching `Runtime` on the classpath, such as `apprise-weld`. 

Support features:


- **dependency injection**

    - pulls in `CDI` and related annotations.
    - pulls in JBoss `Jandex` for annotation indexing (requires a `Maven` plugin execution).


- **logging**

    - pulls in `Logback` as an implementation of `Slf4j`.
    - defines a configuration fragment (`logback-core.xml`) ready for inclusion in `logback.xml`.

- **runtime Lifecycle**

    - `Runtime` is an interface for runtime modules and publishes a `Runtime.Instance` with `start()` and `stop()` hooks for the runtime module. It expects back a `CDI` `beanManager()` from the module. 
    
    - `Startup` and `Shutdown` are application events for interested observers.

    - `Main` is an entrypoint for executable JARs and integration tests:
        
        * it uses a `ServiceLoader` to discover a `Runtime` implementation in the class space. 

        * it then `start()`s it and notifies the `Startup` of the application. 
        
        * it `@Observes` low-level events from `CDI` to notify the `Shutdown` of the application.



- **validation**

    - `CheckDsl` is a fluent Api to check for errors and collect error messages.

    - `Validated` is an interface for domain objects that check and report their own errors. 
        * objects implement `validate()`, or else `validate(Mode)` if validation is contextual to a `Mode`, such as the prefined ones: `Mode.Create`, `Mode.Update`,`Mode.delete`. 

        * `validateNow()` and `validateNow(Mode)` are instead client-facing, and throw a `ValidationException` when `validate()` or `validate(Mode)` do return errors.
    

- **configuration**

    - pulls in the `microprofile-config` APIs.
    - pulls in the `microprofile-openapi` annotations, to annotate models that may part of the documentation of an API built on top.

    - `Banner` is a set of (key,value) pairs that describe distinguished configuration elements.
    
    - `ConfigView` extends `Validated` to mark typed views over configuration data. It also defines an `appendTo(Banner)` hook where views can add descriptions to a `Banner`.

    - an observer at `Startup` gathers all the `ConfigView`s available in the class space, validates them, lets them append descriptions to the `Banner`, and dump the `Banner` on the applicationn log.
    
	- `CoreConfig` is a first `ConfigView` that captures configuration for features in the core module: `app.name`,`app.toggles`,`java.tool.options`.

    - `Toggles` is an element of the `CoreConfiguration` that keeps a set of labels as *feature toggles* for the applications.


* **asynchronous utilities**

    - `WorkContext` is an untyped container for thread-bound resources, with low-level facilities to create, lookup, bind and unbind the context, and to insert, extract, and remove resources into and from it.

    - `WorkTask` wraps a `Callable` or `Runnable` task in order to propagate the `WorkContex` bound to the caller's thread to the thread where the task will execute.

    - `@Async` is an annotation for methods that should execute asynchronously, returning a `Future` result or nothing at all. The annotation serves as an `@InterceptorBinding` for an `AsyncInterceptor` that wraps method invocations as `WorkTask`s  and submits them to an `@AsyncExecutor` thread pool. The pool may be provided by the application or, with lower priority, the runtime.

    - `AsyncResult` is a helper to use in `@Async` methods to wrap a return a value in a `Future`.


- **serialisation**

    - pulls in `jackson-databind` annotations.
    - `@Produces` an `ObjectMapper` that tolerates unknown properties and binds private fields.
    - defines a `Binder<T>` that encapsulates basic usage of the `ObjectMapper`.


- **lifecycle**

    - `Lifecycle<S>` is a partial implementation for a model of domain object lifecycle centered a `Lifecycle.State` `S`.
    - `is()` and `was()` inspect the current `state` and the `previousState` of the object.
    - `initial()` must be overridden with the initial state, and `reset()` resets it.
    - `created` and `lastModified` are readonly timestamps, while `lastModifiedBy` is an identifier, typically a username. The first is preset and read-only, the others are set in sync with `touchedBy()`.
    - `LifecycleEvents` defines general purpose `@Qualifier`s and associated literals for key object lifecycle events: `@Added` and `Added.event`, `@Updated` and `Updated.event`, `@Removed` and `Removed.event`.

- **multi-language text**

    - `Language` is an enumeration of six language codes: `en`,`fr`,`es`,`ar`,`zh`, `ru`.
    - `Multilingual` is a `Comparable` model of text in multiple `Language`s, with facilities to set and retrieve text in a given `Language`s, or to fallback to text in English as the default `Language`.


- **bags**

    - a `Bag` carries serialisable properties of abitrary types under a type-driven read/write API:
       
        - `lookup()` returns a property of a given type, if one exists.
        - `get()` performs a lookup but falls back to a new property minted reflectively from the default constructor.
        - `set()` fetches properties or falls back to new or specific instances, then changes it `with()` a client `Consumer` or leaves it set to the fallback instance (`withDefault()`).
    
    - properties may also be explicitly named in method overloads, e.g. to store two or more of the same type. 
 
    - internally, `Bag`s use an `ObjectMapper` to convert properties just-in-time to and from a `json` tree.
Ø

* **tags**

    * a `Tag` is a distinguished label/marker/discriminant for objects of a given `type`, and a `TagCategory` is a group of `Tag`s of the same type.

    * `Tag`s and `TagCategory`s are identified by `Ref`s comprised of a `key` and a `type`. 

    * `TagCategory`s are `Comparable` and `Validated`, have `Multilingual` names and descriptions, and a `Cardinality` that indicates whether objects can have `any`, `zeroOrOne`, `one`, or `oneOrMany` `Tag`s in that category. `TagCategory`s may also be `predefined` and/or `active`, and they have a `Bag` of `properties`.

    * `Tag`s are also `Comparable` and `Validated`, have `Multilingual` names and descriptions of their own, and of course have a `TypeRef` to their `TagCategory`. `Tag`s may also be `predefined` and have their own `Bag` of `properties`.

    * copy constructors can be used to clone `Tag`s from other `Tag`s, and `TagCategory`s from other `TagCategory`. `updateWith()` can instead be used to copy selected selected poperties. Identifiers or types are *not* copied, lest all `Ref`s to it break. Currently, a `Tag` can't change its category  either.

    * `Tagged` is the interface of objects that have `Tag`s (`tags()`,`has()`).

    * `Binder`s for `Tag` and `TagCategory` are available for injection.

- **utilities**

    - there are utlities to handle exceptions: `swallow()`, `sneak()`, `unwrapAndRethrow()`.
    - there are utilities to summarise exceptions: `storyOf()`, `stackof()`,`causesOf`.
    - there are utilities to measure execution time: `time()`,`etime()`.
