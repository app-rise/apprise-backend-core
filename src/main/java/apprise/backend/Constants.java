package apprise.backend;

public class Constants {

    /**
     * A commmon tag for modules and application that document APIs.
     * <p/>
     * Intended to separate documentation about application-level and internal APIs and models,
     * 
     */
    public static final String docAppTag = "App";

}
