package apprise.backend.mail;

import static java.util.Arrays.asList;
import static lombok.AccessLevel.PRIVATE;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@ToString @NoArgsConstructor(access=PRIVATE)
@Data
public class Email {

	
	public static Email mail(@NonNull Recipient ... recipients) {
		Email email = new Email();
		email.recipients().addAll(asList(recipients));
		return email;
	}
	
	
	
	@NonNull @Getter @Setter
	private String subject;
	
	@Getter
	private List<Recipient> recipients = new ArrayList<>();
	
	
	@Getter
	private List<Attachment> attachments = new ArrayList<>();
	
	
	public Email attach(Attachment ... attachments) {
		
		this.attachments.addAll(asList(attachments));
		
		return this;
	}
	
	@NonNull @Getter 
	private String text;
	
	
	public Email text(String text) {
		this.text = text;
		return this;
	}
	
	public Email text(CharSequence seq) {
		return text(seq.toString());
	}
	
}
