package apprise.backend.mail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@AllArgsConstructor
@Data
public class Recipient {

	public static Recipient to(String name, String address) {
		return new Recipient(name, address);
	}
	
	private String name;
	
	@NonNull
	private String address;
}
