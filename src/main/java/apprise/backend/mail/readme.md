
A single `MailService` wraps the `Simple Java Mail` API to send out `Email` messages. 

The service configures the `Simple Java Mail` API with a `mail.host` and a `mail.port` taken from a `MailConfiguration`.

An `Email` has a subject, a set of `Recipient`s, and a set of `Attachment`s. 

A `Recipient` is a pair of a name and an address, and an `Attachment` is a named `javax.activation.DataSource`.

Overall, the API offers some fluency and a degree of abstraction over the underlying implementation.