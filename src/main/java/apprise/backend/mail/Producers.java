package apprise.backend.mail;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.codemonkey.simplejavamail.Mailer;
import org.codemonkey.simplejavamail.TransportStrategy;

@ApplicationScoped
class Producers {

    @Produces
    Mailer mailer(MailConfig config) {

        var protocol = config.mailProtocol();

        TransportStrategy strategy = null;

        // may vary with provider:
        // 1. SMTPS initiates a TLS session first for the SMTP session to follow, just like HTTPS does for HTTP. 
        // That is, a TLS handshake followed by an exchange of certificates, and THEN the HELO/EHLO.
        // STARTTLS begins with a standard/unencrypted HELO/EHLO, followed by a STARTTLS command to initiate TLS session, 
        // so it uses the standard ports 25/587 (with 587 being preferred for SMTP Auth, and port 25 for unauthenticated relaying on a trusted network).

        switch (protocol) {

            case MailConfig.smtpsProtocol: strategy  = TransportStrategy.SMTP_SSL; break;
            case MailConfig.starttlsProtocol : strategy  = TransportStrategy.SMTP_TLS; break;

            default: 
            
            strategy = TransportStrategy.SMTP_PLAIN;
        }

        return new Mailer(config.mailHost(), config.mailPort(), config.mailUser(),config.mailPwd(), strategy);
    }

}