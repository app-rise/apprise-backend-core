package apprise.backend.mail;

import static apprise.backend.Exceptions.storyOf;
import static javax.mail.Message.RecipientType.TO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.codemonkey.simplejavamail.Mailer;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

public interface MailService {

	void send(Email email);

	@NoArgsConstructor(force = true)

	@Slf4j
	@ApplicationScoped
	public static class Default implements MailService {

		@Inject
		Mailer mailer;

		@Inject
		MailConfig config;


		public void send(@NonNull Email mail) {

			log.trace("mailing {}: '{}'", mail.recipients(), mail.subject());

			try {

				org.codemonkey.simplejavamail.email.Email email = new org.codemonkey.simplejavamail.email.Email();

				email.setFromAddress(config.mailFrom().name(),config.mailFrom().address());

				for (Recipient recipient : mail.recipients())
					if (recipient.address() == null || recipient.address().isEmpty())
						log.warn("cannot send email to {}, as there is no address for it.", recipient);
					else
						email.addRecipient(recipient.name(), recipient.address(), TO);

				if (email.getRecipients().isEmpty())
					log.warn("cannot send email, as it has no valid recipients");

				email.setSubject(mail.subject());
				email.setTextHTML(mail.text());
			
				for (Attachment attachment : mail.attachments())
					email.addAttachment(attachment.name(), attachment.source());

				mailer.sendMail(email);

			}

			catch (Exception tolerate) { //we're not failing a process for this.

				log.error("could not mail {} about '{}' : {}", mail.recipients(), mail.subject(), storyOf(tolerate));

			}
		}
	}
}
