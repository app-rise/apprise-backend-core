package apprise.backend.mail;

import javax.activation.DataSource;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter @RequiredArgsConstructor @ToString
public class Attachment {

	public static NameClause source(final DataSource source) {
		return new NameClause() {
			
			@Override
			public Attachment called(String name) {
				return new Attachment(name, source);	
			}
		};
	}
	
	public static interface NameClause {
	
		Attachment called(String name);
	}
	
	private final String name;
	
	@NonNull
	private final DataSource source;
}
