package apprise.backend.mail;

import static java.lang.String.format;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.Config;

import apprise.backend.config.Banner;
import apprise.backend.config.ConfigView;
import apprise.backend.config.CoreConfig;

@ApplicationScoped
public class MailConfig implements ConfigView {

    static final String mailfrom = "mail.from";
    static final String mailHost = "mail.host";
    static final String mailPort = "mail.port";
    static final String mailUser = "mail.username";
    static final String mailPwd = "mail.pwd";
   
    static final String mailProtocol = "mail.proto";
    static final String smtpProtocol = "smtp";
    static final String smtpsProtocol = "smtps";
    static final String starttlsProtocol = "starttls";

    @Inject
    Config base;

    @Inject
    CoreConfig core;

    public Recipient mailFrom() {

        return base.getOptionalValue(mailfrom, String.class).map(s -> s.split(":"))
                .map(parts -> new Recipient(parts[0], parts[1]))
                .orElse(new Recipient(core.appname(), core.appname() + "@acme.org"));
    }

    public String mailProtocol() {
        return base.getOptionalValue(mailProtocol, String.class).orElse(smtpProtocol);

    }

    public String mailHost() {
        return base.getOptionalValue(mailHost, String.class).orElse(null);

    }


    public Integer mailPort() {
        return base.getOptionalValue(mailPort, Integer.class).orElse(null);

    }

    public String mailUser() {
        return base.getOptionalValue(mailUser, String.class).orElse(null);

    }

    public String mailPwd() {
        return base.getOptionalValue(mailPwd, String.class).orElse(null);

    }

    @Override
    public void appendTo(Banner banner) {
        if (mailHost() != null) {
            banner.append("mail.protocol", mailProtocol());
            banner.append("mail.url", format("%s:%s",mailHost(),mailPort()));
            banner.append("mail.cred", format("%s:%s",mailUser() == null ? "<anon>" : mailUser(),mailPwd()==null ? "<nopwd>" : "<pwd>"));
            banner.append("mail.from", format("%s:%s",mailFrom().name(),mailFrom().address()));
        }
    }

}
