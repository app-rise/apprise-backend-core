package apprise.backend.validation;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;

public interface Exceptions {

	
	// generic carrier about the outcome of a validation processes, typically over models or part of broader processes.

	public static class ValidationException extends RuntimeException {

		@Getter
		private final List<String> issues;
		
		public ValidationException(String msg, List<String> issues) {
			
			super(msg+":\n"+_msgOf(issues));
			this.issues = issues;
		}
		
		public ValidationException(List<String> issues) {
			this("validation errors",issues);
		}
		
		// -- helpers
		private static String _msgOf(List<?> issues) {
			return issues.stream().map(Object::toString).collect(Collectors.joining("\n"));
		}
	}

}
