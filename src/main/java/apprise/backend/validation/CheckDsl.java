package apprise.backend.validation;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import apprise.backend.validation.Exceptions.ValidationException;
import lombok.RequiredArgsConstructor;

/**
 
 	A small DSL for fluent validation and error collection along object hierarchies.
 	

 */
public class CheckDsl {
	
	
	/** Starts collecting checks and errors for a given target object. */ 
	public static <T> ErrorClause<T> given(T instance) {
		return new ErrorClause<T>(instance);
	}
	
	@RequiredArgsConstructor
	public static class ErrorClause<T> {
		
		// target object..
		private final T instance;
		
			
		//	error accumulator.
		List<String> errors = new ArrayList<>();
		
		
		/** checks that the target satisfies a predicate. */
		public OrClause<T> check(Predicate<T> check) {
			
			return ( msg, params) -> checkWith( ()->  check.test(instance) ? emptyList(): singletonList(format(msg,params)) );
			
		
		}
		
		
		public OrClause<T> checkExists(Optional<?> value) {
			
			return check( $ -> value.isPresent());
			
		
		}
		
		
		public interface OrClause<T> {
			
			/** the error message of a failed check. */
			public ErrorClause<T> or(String msg, Object ... params);
			
		}

		
				
		// delegations.
				
		/** checks that the target passes the checks of a validating method. */
		public ErrorClause<T> checkWith(Supplier<List<String>> validator) {
			
			if (guard)
				errors.addAll(validator.get());
			else
				guard =true;
			
			return this;
		}
		
		
		/** propagates the check on the target to one more objects that support validation. */
		public ErrorClause<T> check(Validated ... validated) {
			
			return check(asList(validated));
		}
		
		/** propagates the check on the target to one more objects that support validation. */
		public <S extends Validated> ErrorClause<T> check(Collection<S> validated) {
			
			return checkWith(validated, v -> v.validate());
		}

		/** propagates the check on the target to one more objects that support validation. */
		public <S extends Validated> ErrorClause<T> check(Collection<S> validated, Mode mode) {
	
			return checkWith(validated, v -> v.validate(mode));
		}

		
		
		/** propagates the check on the target to one more objects under a given validator. */
		public <S> ErrorClause<T> checkWith(Collection<S> validated, Function<S,List<String>> validator) {
			
			if (validated!=null)
				validated.stream().filter(v->v!=null).forEach( v-> checkWith( () -> validator.apply(v) ));
			
			return this;
		}
		
		
		/** propagates the check on the target to one more objects under a given validator. */
		public <K,V> ErrorClause<T> checkWith(Map<K,V> validated, BiFunction<K,V,Supplier<List<String>>> validator) {
			
			if (validated!=null)
				validated.entrySet().stream().map(e->validator.apply(e.getKey(), e.getValue())).forEach(this::checkWith);
			
			return this;
		}
		
		
		
		boolean guard = true;
		
		
		public ErrorClause<T> provided(boolean guard) {
			
			this.guard =guard;
			
			return this;
		}
		
		
		
		// terminals.
		
		/** throws a {@link ValidationException} if some checks have failed so far. */
		public void now() {

			if ( !errors.isEmpty() )
				throw new ValidationException(errors);

		}
		
		/** throws a {@link ValidationException} with a given message if some checks have failed so far. */
		public void or(String msg) {
			
			if ( !errors.isEmpty() )
				throw new ValidationException(msg,errors);
		
		}
		
		/** collects all the errors of the checks failed so far. */
		public List<String> andCollect() {
			return errors;			 
		}
				
	}
	
	
}
