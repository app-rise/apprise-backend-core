package apprise.backend.validation;

import lombok.AllArgsConstructor;
import lombok.Data;

public interface Mode { 
	
	@Data
	@AllArgsConstructor
	final static class Predefined implements Mode {
		
		String name;
		
	}
	
	Mode defaultmode = new Predefined("DEFAULT");
	
	Mode create = new Predefined("CREATE");
	Mode update = new Predefined("UPDATE");
	Mode delete = new Predefined("REMOVE");
	
}