package apprise.backend.validation;

import static java.util.stream.Collectors.toList;

import java.util.List;

import apprise.backend.validation.Exceptions.ValidationException;

/**
 * The interface of domain objects that validate their own state, optionally in
 * a {@link Mode} that captures some known context (eg. cf. {@link Mode#create,
 * Mode#update, Mode#update }).
 */
public interface Validated {

	/** Returns the errors, if any, in the current state of this object. */
	default List<String> validate() {

		return validate(Mode.defaultmode);

	}

	/**
	 * Returns the errors, if any, in the current state of this object, and in the
	 * context of a given {@link Mode}.
	 */
	default List<String> validate(Mode mode) {

		throw new IllegalStateException("unsupported mode " + mode);
	}

	default void validateNow() throws ValidationException {

		List<String> errors = validate();

		if (!errors.isEmpty())
			throw new ValidationException(errors);
	}

	default void validateNow(Mode mode) throws ValidationException {

		List<String> errors = validate(mode);

		if (!errors.isEmpty())
			throw new ValidationException(errors);
	}

	// -------------------------------------   static client facilities to validate object collections

	/**
	 * Returns the accumulated errors, if any, in the current state of all the
	 * objects in the collection.
	 */
	static List<String> validate(List<? extends Validated> validated) {

		return validated.stream().flatMap(v -> v.validate().stream()).collect(toList());

	}

	/**
	 * Returns the accumulated errors, if any, in the current state of all the
	 * objects in the collection, in the context of a given {@link Mode}.
	 */
	static List<String> validate(List<? extends Validated> validated, Mode mode) {

		return validated.stream().flatMap(v -> v.validate(mode).stream()).collect(toList());

	}

	static void validateNow(List<? extends Validated> validated) {

		List<String> errors = validate(validated);

		if (!errors.isEmpty())
			throw new ValidationException(errors);
	}

	static void validateNow(List<? extends Validated> validated, Mode mode) {

		List<String> errors = validate(validated, mode);

		if (!errors.isEmpty())
			throw new ValidationException(errors);
	}
}