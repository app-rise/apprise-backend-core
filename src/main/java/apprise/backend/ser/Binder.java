package apprise.backend.ser;

import java.io.InputStream;
import java.io.OutputStream;

import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

/**
 	Encapsulates bidirectional JSON transformations for arbitrary models. 
 	<p>
 	Typically made available for injection with {@link Produces} methods that use the's application's {@link ObjectMapper}.
 	
<pre>
&#064;Produces
Binder<MyModel> myBinder(ObjectMapper mapper) {
 return new Binder<>(mapper, MyModel.class);
}

</pre>
 */

@RequiredArgsConstructor
@NoArgsConstructor(force=true) // required for proxied injection
public class Binder<T> {

		private final ObjectMapper mapper;
		private final Class<T> type;
		
		
		@SneakyThrows
		public T bind(InputStream stream) {
			
			return mapper.readValue(stream,type);
		}

		@SneakyThrows
		public T bind(byte[] bytes) {
			
			return mapper.readValue(bytes,type);
		}
		
		@SneakyThrows
		public <S extends OutputStream> S bind(T t, S stream) {
			mapper.writeValue(stream,t);	
			return stream;
		}
		
		@SneakyThrows
		public String bind(T t) {
			
			return mapper.writeValueAsString(t);
		}
		
		@SneakyThrows
		public T bind(String content) {
			
			return mapper.readValue(content,type);
			
		}

}
