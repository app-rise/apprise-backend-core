package apprise.backend.ser;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.annotation.PropertyAccessor.FIELD;
import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.MapperFeature.AUTO_DETECT_FIELDS;
import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_EMPTY_BEANS;
import static com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@ApplicationScoped
public class Producers {

	private static ObjectMapper mapper = new ObjectMapper()
			.registerModule(new Jdk8Module())
			.registerModule(new JavaTimeModule())
			.disable(WRITE_DATES_AS_TIMESTAMPS)
			.disable(WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS)
			.disable(READ_DATE_TIMESTAMPS_AS_NANOSECONDS)
			.disable(ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
			.configure(FAIL_ON_EMPTY_BEANS, false)
			.configure(FAIL_ON_IGNORED_PROPERTIES, false)
			.configure(FAIL_ON_UNKNOWN_PROPERTIES, false)
			.configure(AUTO_DETECT_FIELDS, true)
			.configure(INDENT_OUTPUT, true)
			.setSerializationInclusion(NON_NULL)
			.setVisibility(FIELD, ANY);

	@Produces // dependent, mapper is not proxiable
	public static ObjectMapper mapper() {
		return mapper;
	}

}
