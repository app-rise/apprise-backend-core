package apprise.backend.async;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class WorkContext {

    private static final ThreadLocal<WorkContext> contexts = new ThreadLocal<>();


    /**
     * Returns the context bound to the current thread, if any exists, othriwse binds and returns
     * a new one.
     */
    public static WorkContext currentOrNew() {

        // note: using orElse() generates a subtle bug.
        // since evaluation is eager, a new context would be bound
        // to the current thread even if one already exists and is returned.
        return current().orElseGet(()->new WorkContext().bind());

    }

    public static Optional<WorkContext> current() {

        return Optional.ofNullable(contexts.get());
    }


    final Instant start = Instant.now();
    final Map<String, Object> data = new HashMap<>();

    /** Binds context to current thread. */
    public WorkContext bind() {

        contexts.set(this);

        return this;
    }

    public WorkContext unbind() {

        contexts.remove();

        return this;
    }

    public WorkContext set(@NonNull String key, Object value) {

        data.put(key, value);

        return this;
    }

    public WorkContext set(@NonNull Object value) {

        return this.set(value.getClass(),value);
    }

    public <T> WorkContext set(@NonNull Class<T> type, @NonNull Object value) {

        return this.set(type.getCanonicalName(),value);
    }


    public <T> T get(String key, Class<T> type) {

        return type.cast(data.get(key));
    }

    public <T> T get(Class<T> type) {

        return this.get(type.getCanonicalName(),type);
    }


    public WorkContext remove(String key) {

        data.remove(key);

        return this;
    }

    public WorkContext remove(Class<?> type) {

        return this.remove(type.getCanonicalName());
    }
}
