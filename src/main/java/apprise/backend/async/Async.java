package apprise.backend.async;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;
import javax.interceptor.InterceptorBinding;

@InterceptorBinding
@Qualifier
@Target({TYPE,METHOD,FIELD})
@Retention(RUNTIME)
@Inherited
public @interface Async {

}
