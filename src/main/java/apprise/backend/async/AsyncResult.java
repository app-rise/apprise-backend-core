package apprise.backend.async;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName="of")
public final class AsyncResult<V> implements Future<V> {

    private final V result;

    @Override 
    public boolean cancel(boolean mayInterruptIfRunning) {
    	throw new IllegalStateException("not a real future");
    }

    @Override
    public boolean isCancelled() {
    	throw new IllegalStateException("not a real future");
    }

    @Override
    public boolean isDone() {
    	throw new IllegalStateException("not a real future");
    }

    @Override
    public V get() {
	    return result;
    }

    @Override
    public V get(long timeout, TimeUnit unit)  {
    	throw new IllegalStateException("not a real future");
    }

}