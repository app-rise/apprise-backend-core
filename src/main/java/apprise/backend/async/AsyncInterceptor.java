package apprise.backend.async;

import static javax.interceptor.Interceptor.Priority.LIBRARY_BEFORE;

import java.io.Serializable;
import java.util.concurrent.ExecutorService;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;



@Interceptor
@Async

// comes before executors that use thread-local resources, so that resources are
// bounded to the pool thread.
@Priority(LIBRARY_BEFORE - 100)


/**  Intercepts invocations to {@link Async} methods and submits as {@link WorkTask}s to the {@AsyncExecutor} service. */
public class AsyncInterceptor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    @AsyncExecutor
    ExecutorService asyncExecutor;

    @AroundInvoke
    public Object submitAsync(InvocationContext ctx) throws Exception {

    
        var task =  WorkTask.of(()->ctx.proceed());

        var future = asyncExecutor.submit(task);

        return new FutureWrapper(future);
    }
}