
We emulate the `@Asynchronous` methods of enterprise runtimes with an analoguous approach:


- a custom `@Async` annotation for methods, defined as an `@InterceptorBinding` to match a corresponding `AsyncInterceptor`. The `AsyncInterceptor` submits method invocations to an `Executor` provided by the runtime, typically with low priority (so that it can be overridden by the application). 
(This is the same `ExecutorService` that `Jersey` uses to do async request processing.)


- a technical `FutureWrapper` utility used by the `AsyncInterceptor` to lazily unwrap the double `Future` returned by the `ExecutorService` when `@Async` methods are not `void` methods and return `Future`s of their own.

- an `AsyncResult`utility to use in `@Async` methods that return a value in order to easily wrap the value in a (fake) `Future`. Eg:

```java
	return AsyncResult.of(..some value...)
```

**Note**: `@AsyncInterceptor` has higher priority than other support interceptors  (`LIBRARY_BEFORE`-100). This ensures that interceptors that use thread-local resources bind them to the thread where the method executes, rather then the thread of the caller.