package apprise.backend.async;

import java.util.Optional;
import java.util.concurrent.Callable;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/* Wraps a {@link Callable} or {@link Runnable} to propagate the {@link WorkContex} bound to the caller thread to the execution thread.   */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class WorkTask<T> implements Callable<T> {

    public static <T> WorkTask<T> of(Callable<T> callable) {

        return new WorkTask<>(WorkContext.current(), callable);
    }

    public static WorkTask<Void> of(Runnable runnable) {

        return of(() -> runnable.run());
    }

    final Optional<WorkContext> wc;
    final Callable<T> callable;

    @Override
    public T call() throws Exception {

        // propagate client's work context, if any, to this thread.
        wc.ifPresent(WorkContext::bind);

        try {

            return callable.call();

        }

        finally {

            //  unbind in all cases: success/failure, 
            //  propagated from client thread or bound in this thread.
            WorkContext.currentOrNew().unbind();
        }

    }

}