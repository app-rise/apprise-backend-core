package apprise.backend.session;

import javax.enterprise.context.RequestScoped;

import lombok.Data;

@RequestScoped
@Data
public class SessionService {
    
    Session currentSession;
}
