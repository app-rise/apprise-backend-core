package apprise.backend.session;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class Producer {

    @Produces @RequestScoped
    static Session currentSession( SessionService service) {

        return Optional.ofNullable(service.currentSession()).orElse(Session.none);

    }
    
}
