package apprise.backend.session;

import lombok.Data;

@Data
public class Session {

    public static final Session none = new Session();

    String id;
    
}
