package apprise.backend.tag;

import static apprise.backend.tag.TagCategory.State.active;
import static java.lang.String.format;

import java.util.List;

import apprise.backend.model.Bag;
import apprise.backend.model.Lifecycle;
import apprise.backend.model.Lifecycled;
import apprise.backend.model.Multilingual;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TagCategory implements Lifecycled<TagCategory.State, TagCategory>, Validated, Comparable<TagCategory> {

	public static enum State {
		active, inactive
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Cardinality {

		public static final Cardinality any = new Cardinality();
		public static final Cardinality zeroOrOne = new Cardinality(0, 1);
		public static final Cardinality one = new Cardinality(1, 1);
		public static final Cardinality atLeastOne = new Cardinality(1, null);

		Integer min;
		Integer max;

		public Cardinality(Cardinality other) {
			min(other.min()).max(other.max());
		}
	}

	String id;
	String type;
	Cardinality cardinality;

	boolean guarded;
	boolean predefined;

	Lifecycle<State> lifecycle = new Lifecycle<>(State.inactive);

	Multilingual name;
	Multilingual description;

	Bag properties = new Bag();

	public TagCategory(TagCategory other) {

		updateWith(other)
				.id(other.id())
				.type(other.type())
				.predefined(other.predefined())
				.guarded(other.guarded())
				.cardinality(new Cardinality(other.cardinality))
				.lifecycle(new Lifecycle<>(other.lifecycle()));
	}

	public TagCategory copy() {

		return new TagCategory(this);
	}

	public TagCategory updateWith(TagCategory other) {

		// these can change at all times
		name(new Multilingual(other.name()))
				.description(new Multilingual(other.description()))
				.properties(other.properties());

		// we guard some changes if category is predefined or is-and-remains required.
		// this allows required to change along with other guarded properties.
		var isProtected = this.predefined || (guarded && other.guarded);

		if (!isProtected)
			cardinality(new Cardinality(other.cardinality))
			.lifecycle().updateWith(other.lifecycle());

		// this can also change at all time
		guarded(other.guarded());
	
		return this;

	}

	@Override
	public int compareTo(TagCategory o) {
		return name.compareTo(o.name());
	}

	@Override
	public List<String> validate(Mode mode) {
		return CheckDsl.given(this).check($ -> id != null).or("id is missing")
				.check($ -> name != null).or("name is missing")
				.check($ -> type != null).or("type is missing")
				.provided(name != null).check($ -> name.inDefaultLanguage() != null).or("no name in default language")
				.provided(mode == Mode.delete).check($ -> !predefined() && !guarded() && state() != active)
				.or("cannot remove category %s because it is active, predefined, or protected", ref())
				.andCollect();
	}

	public String ref() {
		return format("%s@%s", id, type);
	}

}
