package apprise.backend.tag;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {

	
	@Produces @ApplicationScoped
	Binder<Tag> tagBinder(ObjectMapper mapper) {
		return new Binder<>(mapper,Tag.class);
	}
	
	@Produces @ApplicationScoped
	Binder<TagCategory> tagCategoryBinder(ObjectMapper mapper) {
		return new Binder<>(mapper,TagCategory.class);
	}
	
}
