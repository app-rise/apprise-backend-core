package apprise.backend.tag;

import static java.util.stream.Collectors.toSet;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Stream;

public interface Tagged<T> {

	
	Set<String> tags();


	T tags(Collection<String> tags);
	
	default T tags(Tag... tags) {
		return tags(Stream.of(tags).map(Tag::id).collect(toSet()));
	}

	default boolean has(String...tags) {
		return Stream.of(tags).allMatch(tags()::contains);
	}
	
	default boolean has(Tag...tags) {
		return has(Stream.of(tags).map(Tag::id).toArray(String[]::new));
	}
}
