package apprise.backend.tag;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.enterprise.util.TypeLiteral;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagExpression {

    static TypeLiteral<List<Tag>> tagstype = new TypeLiteral<List<Tag>>() {
    };

    public static enum Op {

        anyOf, allOf, noneOf
    }

    @Data
    @NoArgsConstructor
    public static class Term {

        public Term(Term other) {

            category(other.category)
                    .tags(new ArrayList<>(other.tags))
                    .op(other.op);
        }

        String category;
        List<String> tags = new ArrayList<>();
        Optional<Op> op = Optional.empty();

    }

    List<Term> terms = new ArrayList<>();

    public TagExpression(TagExpression other) {

        terms(other.terms().stream().map(Term::new).collect(toList()));
    }

    public boolean matches(Tagged<?> tagged) {

        return terms.stream().allMatch(t -> evaluate(tagged, t));

    }

    private boolean evaluate(Tagged<?> tagged, Term term) {

        long matchCount = term.tags().stream() 
                .filter(tag -> tagged.tags().contains(tag))
                .count();

        
        var op = term.op().orElse(Op.anyOf);

        if (op==Op.noneOf)
            return matchCount == 0;

        if (op==Op.anyOf)
            return matchCount > 0;

        return matchCount == term.tags.size();
 

    }

}
