package apprise.backend.tag;

import static apprise.backend.tag.Tag.State.active;
import static java.lang.String.format;

import java.util.List;

import apprise.backend.model.Bag;
import apprise.backend.model.Lifecycle;
import apprise.backend.model.Lifecycled;
import apprise.backend.model.Multilingual;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Tag implements Lifecycled<Tag.State, Tag>, Validated, Comparable<Tag> {

	public static enum State {
		active, inactive
	}

	String id;
	String type;

	Lifecycle<State> lifecycle = new Lifecycle<>(State.inactive);

	String category;

	boolean guarded;
	boolean predefined;
	
	Multilingual name = new Multilingual();
	Multilingual shortname = new Multilingual();
	Multilingual description = new Multilingual();

	String code;

	Bag properties = new Bag();

	public Tag(Tag other) {

		updateWith(other)
				.id(other.id)
				.type(other.type)
				.category(other.category)
				.guarded(other.guarded)
				.predefined(other.predefined);
	}

	public Tag copy() {
		return new Tag(this);
	}

	public Tag updateWith(Tag other) {

		// these can change at all times
		name(new Multilingual(other.name()))
				.shortname(other.shortname())
				.description(new Multilingual(other.description()))
				.properties(other.properties())
				.code(other.code);

		// we guard some changes if tag is predefined or is-and-remains guarded.
		// this allows required to change along with other guarded properties.
		var isProtected = this.predefined || (guarded && other.guarded);

		if (!isProtected)
			category(guarded ? category : other.category)
					.lifecycle().updateWith(other.lifecycle());

		// this can also change at all time, provided the object is not predefined
		guarded(other.guarded());

		return this;
	}

	public String ref() {

		return format("%s@%s%s", id, type, category == null ? "" : format(" (%s)", category));
	}

	@Override
	public List<String> validate(Mode mode) {

		return CheckDsl.given(this).check($ -> id != null).or("id is missing")
				.check($ -> name != null).or("name is missing")
				.check($ -> type != null).or("type is missing")
				.provided(type != null && category != null)
				.provided(name != null)
				.check($ -> name.inDefaultLanguage() != null).or("no name in default language")
				.provided(mode == Mode.delete).check($ -> !predefined() && !guarded() && state() != active)
				.or("cannot remove tag %s because it is active, predefined, or protected", ref())
				.andCollect();
	}

	@Override
	public int compareTo(Tag o) {
		return name.compareTo(o.name());
	}

}
