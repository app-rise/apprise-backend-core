package apprise.backend.tag;

public class Constants {
    
    public static final String tagType="tag";
    public static final String categoryType="cat";
    
    public static final String tagPrefix="TG";
    public static final String categoryPrefix="TGC";
}
