package apprise.backend.runtime;

import javax.enterprise.inject.spi.BeanManager;

public interface Runtime {

        RuntimeInstance instance () ;
    
        public static interface RuntimeInstance {
    
            void start();
            
            void stop();
    
            BeanManager beanManager();
    
        }
    
}
