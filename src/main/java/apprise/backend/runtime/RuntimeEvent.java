package apprise.backend.runtime;

import lombok.NoArgsConstructor;

public interface RuntimeEvent {
	
	@NoArgsConstructor(staticName="now")
	public static class Startup implements RuntimeEvent {
		
	}
	
	@NoArgsConstructor(staticName="now")
	public static class Shutdown implements RuntimeEvent {
		
	}

}
