package apprise.backend.runtime;

import java.util.Optional;
import java.util.ServiceLoader;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.BeanManager;

// import org.slf4j.bridge.SLF4JBridgeHandler;

import apprise.backend.runtime.Runtime.RuntimeInstance;
import apprise.backend.runtime.RuntimeEvent.Shutdown;
import apprise.backend.runtime.RuntimeEvent.Startup;
import lombok.extern.slf4j.Slf4j;

/**
 * Starts the application and returns an interface to its {@link Runtime}.
 * <p/>
 * Notifies {@link Startup} and {@link Shutdown} application observers before
 * the runtime starts and before it goes down.
 * <p/>
 * Can serve as the entry point of runnable application JARs. It can also be
 * used programmatically, eg. for integration testing.
 */
@Slf4j
@ApplicationScoped
public class Main {

    // stored to be @Produce-d.
    static Optional<RuntimeInstance> instance = Optional.empty();

    @Produces
    @ApplicationScoped
    static RuntimeInstance runtime() {

        return instance.orElseThrow(() -> new IllegalStateException("no runtime as yet"));
    }

    // stored to be invoked at shutdown (RuntimeInstance by then won't return it
    // anymore).
    static Optional<BeanManager> bm = Optional.empty();

    /**
     * Boots up the runtime as a Java application, typically from the Manifest of
     * their runnable JARs.
     */
    public static void main(String[] args) {

        start();

    }

    /**
     * Boots up the runtime.
     * 
     */
    public static RuntimeInstance start() {

        log.info("application is starting");

        RuntimeInstance instance = ServiceLoader.load(Runtime.class).findFirst()
                .orElseThrow(() -> new IllegalStateException("No runtime implementation on the classpath")).instance();

        // must start the runtime to send a first event.
        instance.start();

        BeanManager bm = instance.beanManager();

        try {

            bm.fireEvent(Startup.now());

        } catch (Throwable t) {

            //  a failure in startup sequence may require cleanup (eg. db cleanup in tests)
            //  need to notify observers before we rethrow and lose the bean manager.
            shutdown(bm);
            
            throw t;

        }

        Main.bm = Optional.of(bm);
        Main.instance = Optional.ofNullable(instance);

        return instance;
    }

    // Listens for CDI shutdown and informs application observers.
    static void shutdown(@Observes @Destroyed(ApplicationScoped.class) Object event) {

        bm.ifPresent(Main::shutdown);

    }

    static void shutdown(BeanManager bm) {

        log.info("application is shutting down");
        bm.fireEvent(Shutdown.now());

    }
}
