package apprise.backend.i18n;

import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.concurrent.CompletableFuture.supplyAsync;
import static java.util.stream.Collectors.toList;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.config.CoreConfig;
import apprise.backend.i18n.Translation.Request;
import lombok.SneakyThrows;

@ApplicationScoped
public class RemoteTranslationService implements TranslationService {

    public static final int timeoutInSeconds = 3;

    // used for initialisation and public calls, no need to configure with a builder.
    HttpClient client = HttpClient.newHttpClient();

    @Inject
    CoreConfig config;

    @Inject
    ObjectMapper mapper;

    @Override
    @SneakyThrows
    public <T> T t(Request<T> trequest) {

        var request = HttpRequest.newBuilder()
                .timeout(Duration.of(timeoutInSeconds, SECONDS))
                .setHeader("Content-Type", "application/json")
                .uri(URI.create(config.i18nUrl()))
                .POST(BodyPublishers.ofByteArray(mapper.writeValueAsBytes(trequest)))
                .build();

        var response = client.send(request, BodyHandlers.ofString());

        var status = response.statusCode();

        if (status >= 400)
            throw new IllegalArgumentException(response.body());

        return trequest.parse(response.body());
    }

    @SneakyThrows
    public <T> List<T> t(List<Request<T>> trequests) {

        return trequests.stream().map(r -> supplyAsync(()-> t(r))).map(CompletableFuture::join).collect(toList());

    }
}
