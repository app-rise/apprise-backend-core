package apprise.backend.i18n;

import java.util.List;

import apprise.backend.i18n.Translation.Request;

public interface TranslationService {

    <T> T t(Translation.Request<T> request) throws IllegalArgumentException;

    <T> List<T> t(List<Request<T>> trequests);

}
