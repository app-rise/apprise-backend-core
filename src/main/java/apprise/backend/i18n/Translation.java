
package apprise.backend.i18n;

import static lombok.AccessLevel.PROTECTED;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import apprise.backend.model.Multilingual;
import apprise.backend.model.Multilingual.Language;
import apprise.backend.ser.Producers;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;

@SuppressWarnings("unused")
public interface Translation {

    static class Arguments extends BaseArguments<Arguments> {
    }

    @NoArgsConstructor(force = true)
    @Data
    public abstract class BaseArguments<T extends BaseArguments<T>> {

        String key;

        Map<String, Object> options;

        Map<String, Object> params = new HashMap<>();

       @SuppressWarnings("unchecked")
        public T key(String key) {

            this.key = key;

            return (T) this;
        }

        @SuppressWarnings("unchecked")
        private  T add(String name, Object value) {

            params.put(name, value);

            return (T) this;
        }

        @SuppressWarnings("unchecked")
        public T options(Map<String, Object> options) {

            this.options = options;

            return (T) this;
        }

        @SuppressWarnings("unchecked")
        public T params( Map<String, Object> params) {

            this.params=params;

            return (T) this; 
        }
  
        
        public T param(String name, String value) {

            return add(name, value);
        }

        public T param(String name, Multilingual value) {

            return add(name, value);
        }

        public T param(String name, Arguments value) {

            return add(name, value);
        }

    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    @NoArgsConstructor(force = true)
    static abstract class Request<T> extends BaseArguments<Request<T>> {

        @JsonProperty("lng")
        @Setter(PROTECTED)
        Language language;

        Boolean template;

        abstract T parse(String response);

        public static class Fixed extends Request<String> {

            Fixed(Language l) {

                language(l);
            }

            @Override
            String parse(String response) {
                return response;
            }
        }

        public static class Multi extends Request<Multilingual> {

            @Override
            @SneakyThrows
            Multilingual parse(String response) {
                return Producers.mapper().readValue(response, Multilingual.class);
            }
        }

    }

    // factory methods, hide complexity of interface.

    static Translation.Request<Multilingual> request(String key) {

        return new Translation.Request.Multi().key(key);
    }

    static Translation.Request<String> fixedRequest(String key, Language language) {

        return new Translation.Request.Fixed(language).key(key);
    }
}
