package apprise.backend;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Function;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * Exceptions that recur across applications and can be handled uniformly at network boundaries.
 * 
 */
@Slf4j
public class Exceptions {

	//	use carefully
	public static boolean swallow(String msg, ExceptionalRunnable task) {

		try {
			task.run();
			return false;
		} catch (Throwable t) {
			log.warn("{}:{}", msg, storyOf(t));
			return true;
		}

	}

	public static void sneak(ExceptionalRunnable task) {

		try {
			task.run();
		} catch (RuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}

	}

	//	returns the same 

	@SneakyThrows
	public static void unwrapAndRethrow(Throwable fault, Class<? extends Throwable> exceptiontype) {

		Throwable t = fault;
		while (t != null && !(exceptiontype.isInstance(t)))
			t = t.getCause();

		throw t == null ? fault : exceptiontype.cast(t);
	}

	public static <T> T sneak(Callable<T> task) {

		try {
			return task.call();
		} catch (RuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}

	}

	public static <S,T> Function<S,T> sneak(ExceptionalFunction<S,T> task) {

		return s -> {

			try {
				
				return task.apply(s);

			} catch (RuntimeException e) {
				throw e;
			} catch (Throwable t) {
				throw new RuntimeException(t);
			}
		};

	}

	public static interface ExceptionalRunnable {

		void run() throws Exception;
	}


	public static interface ExceptionalFunction<S,T> {

		T apply(S s) throws Exception;
	
	}


	public static List<String> causesOf(Throwable t) {

		List<String> msgs = new ArrayList<>();

		msgs.add(format("%s @ %s", t.getMessage() == null ? t.getClass()
				.toString() : t.getMessage(), t.getStackTrace()[0].toString()));

		if (t.getCause() != null)
			msgs.addAll(causesOf(t.getCause()));

		return msgs;

	}

	public static String storyOf(Throwable t) {

		return storyOf(t, "<<");

	}

	public static String storyOf(Throwable t, String delimiter) {

		return causesOf(t).stream().collect(joining(format(" %s ", delimiter)));

	}

	public static String stackOf(Throwable t) {

		StringWriter writer = new StringWriter();
		t.printStackTrace(new PrintWriter(writer, true));
		return writer.toString();
	}

	//	separates lookup failures from other illegal states that may occur deep down a call stack.
	//	in particular, allows specific 404 mappings in api barriers, when entities correspond to REST resources. 

	public static class NoSuchEntityException extends IllegalStateException {

		public NoSuchEntityException(String msg) {

			super(msg);
		}

		public NoSuchEntityException(String msg, Throwable cause) {

			super(msg, cause);
		}

	}

	public static final String ioExceptionPrefix = "I/O failure:\n'%s'";

}
