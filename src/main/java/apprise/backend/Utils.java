package apprise.backend;

import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.lang.Thread.currentThread;
import static java.util.stream.IntStream.range;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

import javax.enterprise.event.Event;
import javax.enterprise.inject.spi.CDI;
import javax.enterprise.util.TypeLiteral;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Utils {

	
	public static <T> T lookup(Class<T> type) {
		
		return CDI.current().select(type).get();
	}

	public static <T> Event<T> lookupEvent(Class<T> type) {

		TypeLiteral<Event<T>> events = new TypeLiteral<Event<T>>(){};
    		
		return CDI.current().select(events).get();

	}
	

	// performs but won't count warmup
	public static long time(int times, String name, Runnable task) {
		
		time(format("%s (warmup)",name),task);
		
		long avg = range(0, times).mapToLong(n -> time(format("%s (run n.%s)",name,n+1),task) ).sum()/times;
		log.trace("{} took averagely {} ms",name,avg);
		return avg;
	}
	
	public static long time(String msg, Runnable task) {
		

		log.info("{}...",msg);
		
		long time = currentTimeMillis();
		
		try {
		
			task.run();
		}
		finally {
		
			time =  currentTimeMillis()-time;
			log.trace("{} took {} ms",msg,time);
		
		}
		
		return time;
	}
	
	@SneakyThrows
	public static long etime(String msg,ExceptionalRunnable task) {
		
		long time = currentTimeMillis();

		try {
			task.run();
		}
		finally {

			time =  currentTimeMillis()-time;
			log.trace("{} took {} ms",msg,time);
			
		}
		
		return time;

	}
	
	@SneakyThrows
	public static <T> T time(String msg, Callable<T> task) {
		
		long time = currentTimeMillis();
		T result = task.call();
		time =  currentTimeMillis()-time;
		log.trace("{} took {} ms",format(msg,result),time);
		return result;
	}
	
	@SneakyThrows
	public static <T> T time(String msg,Task<T> task) {
		
		long time = currentTimeMillis();
		AtomicLong progress = new AtomicLong(time);
		
		Timer timer = n -> {
			long now = currentTimeMillis();
			log.trace("{}:'{}' took {} ms",currentThread().getStackTrace().length-1,n ,now-progress.get());
			progress.set(now);
		};
		
		T result = task.time(timer);
		
		log.trace("{}:'{}' in total took {} ms",currentThread().getStackTrace().length+1,format(msg,result) ,currentTimeMillis()-time);
		
		return result;
		
		
		
	}
	
	/** how long does it take? **/
	public static long time(Runnable task) {
		
		return time("task",task);
	}
	
	/////////////////////////
	
	public static interface Timer {
		
		void click(String name);
	}
	
	public static interface Task<T> {
		
		T time(Timer t) throws Exception;
	}
	

	public static interface ExceptionalRunnable {
		
		void run() throws Exception;
	}


}
