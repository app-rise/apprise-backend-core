package apprise.backend.config;

import static javax.interceptor.Interceptor.Priority.LIBRARY_BEFORE;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;

import apprise.backend.runtime.RuntimeEvent.Startup;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

	// dumps configuration on startup
	public void printBanner(@Observes @Priority(LIBRARY_BEFORE - 10) Startup event, Instance<ConfigView> views) {

		Banner banner = new Banner();

		views.forEach(v -> v.appendTo(banner));
		
		log.info("environment configuration:\n{}",banner);

		try {
			views.forEach(ConfigView::validateNow);
		}
		catch(RuntimeException e) {

			log.error("invalid configuration",e);
			
			throw e;
		}
	}
}
