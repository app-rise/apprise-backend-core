package apprise.backend.config;

import static java.util.Collections.emptyList;

import java.util.List;

import apprise.backend.validation.Validated;

public interface ConfigView extends Validated {

    default void appendTo(Banner banner) {

    }

    @Override
    default List<String> validate() {
        return emptyList();
    }
    
}
