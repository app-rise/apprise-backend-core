package apprise.backend.config;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.Config;

import apprise.backend.model.Multilingual.Language;
import lombok.Getter;
import lombok.Setter;

@ApplicationScoped
public class CoreConfig implements ConfigView {

	static final String javaopts = "java.tool.options";
	static final String appname = "app.name";
	static final String toggleprop = "app.toggles";
	static final String i18nUrl = "i18n.url";
	static final String supportedLanguages = "app.langs";
	static final String fallbackLanguage = "app.fallback_lang";

	@Inject
	Config base;

	@Getter @Setter
	Toggles toggles;

	@PostConstruct
	public void init() {

		resetToggles();
	}

	

	public String appname() {

		return base.getOptionalValue(appname, String.class).orElse("apprise-app");
	}

	public String i18nUrl() {

		return base.getOptionalValue(i18nUrl, String.class).orElse("http://localhost:9999");
	}

	public String javaopts() {

		return base.getOptionalValue(javaopts, String.class).orElse(null);

	}

	@SuppressWarnings("all")
	public List<Language> supportedLanguages() {

		return base.getOptionalValue(supportedLanguages, String.class)
				   .map(array-> Stream.of(array.split(",")).map(Language::valueOf).collect(toList()))
				  .orElse(asList(Language.values()));

	}

	public Language fallbackLanguage() {

		return base.getOptionalValue(fallbackLanguage, Language.class).orElse(Language.en);

	}
	

	public void resetToggles() {

		toggles=base.getOptionalValue(toggleprop, String.class).map(v -> asList(v.split(","))).map(Toggles::new)
		.orElse(new Toggles());
	}

	@Override
	public void appendTo(Banner banner) {
		banner.append("name", appname());
		banner.append("toggles", toggles().all().stream().collect(joining(",")));
		banner.append("languages", format("%s (%s)", supportedLanguages(), fallbackLanguage()));
		banner.append(i18nUrl, format(i18nUrl()));
	}

}
