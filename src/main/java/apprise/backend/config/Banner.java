package apprise.backend.config;

import static java.lang.String.format;

import java.util.LinkedHashMap;
import java.util.Map;

import lombok.EqualsAndHashCode;

public class Banner {

    @EqualsAndHashCode.Exclude
    Map<String, String> entries = new LinkedHashMap<>();

    public Banner append(String prop, String message) {

        entries.put(prop, message);
        return this;

    }

    public String toString() {

        StringBuilder banner = new StringBuilder();

        String prop = "%s: %s\n";

        banner.append("\n************************************************************************************\n");

        entries.forEach((k, v) -> banner.append(format(prop, k, v)));

		banner.append("************************************************************************************\n");

		return banner.toString();
	}
    
}
