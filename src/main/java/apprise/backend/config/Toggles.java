package apprise.backend.config;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@ToString
public class Toggles {

	public static final String dev = "DEV";

	final List<String> toggles = new ArrayList<>();

	public Toggles(List<String> toggles) {

		toggles.forEach(t -> this.toggles.add(t.toUpperCase()));

	}

	public void reset() {

		this.toggles.clear();;
	}

	public void add(String... toggles) {

		this.add(asList(toggles));
	}

	public void add(Collection<String> toggles) {

		this.toggles.addAll(toggles);
	}
	
	
	public boolean isActive(String toggle) {
		return toggles.contains(toggle.toUpperCase());
	}
	
	
	public List<String> all() {
		return toggles;
	}
	
	
}
