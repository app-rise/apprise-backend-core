package apprise.backend.model;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {


    @Produces @ApplicationScoped
    Binder<Bag> bagbinder(ObjectMapper mapper) {

        return new Binder<>(mapper, Bag.class);
    } 
}
