package apprise.backend.model;

import java.util.Set;

/**
 *  The interface of objects that can be predefined, or predefine some of their properties.
 *  <p/>
 *  Predefined objects are required by the application. Typically, they're staged early and cannot be removed.
 *  And some of their properties may be predefined in turn and not allowed to change.
 */
public interface Predefined<P, SELF extends Predefined<P,SELF>> {

	
	boolean predefined();
	
	SELF predefined(boolean val);


	Set<P> predefinedProperties();


	@SuppressWarnings("unchecked")
	default SELF predefine(P property) {

		predefinedProperties().add(property);

		return (SELF) this;
	}

	default boolean predefined(P property) {

		return predefinedProperties().contains(property);
	}
	
	
}
