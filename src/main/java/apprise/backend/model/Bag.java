package apprise.backend.model;

import static apprise.backend.Exceptions.sneak;
import static apprise.backend.ser.Producers.mapper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import javax.enterprise.util.TypeLiteral;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.validation.Exceptions.ValidationException;
import apprise.backend.validation.Validated;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.Value;

/** 
 *  Carries serialisable properties of abitrary types under a type-driven read/write API.
 *  <ul>
 *  <li> <code>lookup()</code> returns a property of a given type, if one exists.</li> 
 * 	<li> <code>get()</code> performs a a lookup but falls back to a new property minted reflectively from the default constructor.
 * 	<li> <code>set()</code> fetch a property or falls back to either a new or a specific instance, then changes it <code>with()</code> a client {@link Consumer}
 * 	or leaves it set to the fallback instance (<code>withDefault()</code>).
 *  <li> properties may also be named in method overloads, e.g. to store two or more with the same type. 
 *  </ul>
 *  Internally, an {@link ObjectMapper} converts properties to and from a serialisable tree model.
 */


@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class Bag implements Iterable<Bag.Entry> {

	@Value
	class Entry {
		String name; 
		JsonNode value;
	}

	@JsonValue // avoids unncessary nesting in JSON serialization	
	Map<String,JsonNode> values = new HashMap<>();
	
	
	// used only for json deserialization, matches @JsonValue above
	@JsonCreator
	private Bag(Map<String,JsonNode> values) {
		this.values=values;
	}

	public Bag(Bag other) {
		this.values=new HashMap<>(other.values);
	}
	

	/**
	 * Returns the anonymous property of a given type, if one exists.
	 * */
	public <T> Optional<T> lookup(Class<T> type) {
		
		return lookup(nameOf(type),type);
	}
	/**
	 * Returns a named property of a given type, if one exists.
	 * */
	public <T> Optional<T> lookup(String name,Class<T> type) {
		
		return Optional.ofNullable(values.get(name)).map(t -> sneak(()-> mapper().treeToValue(t,type)));
	}

	public <T> Optional<T> lookup(String name, TypeLiteral<T> type) {


		JavaType jt = mapper().getTypeFactory().constructType(type.getType());

//		mapper.readValue(mapper().treeAsTokens(node), jt);

		
		return Optional.ofNullable(values.get(name)).map(t -> sneak(()-> mapper().readValue(mapper().treeAsTokens(t), jt)));
	}


	public boolean contains(String name) {
		
		return lookup(name,Object.class).isPresent();
	}
	
	
	
	/**
	 * Returns the anonymous property of a given type or a brand new instance.
	 * */
	public <T> T get(Class<T> type) {
		
		return get(nameOf(type),type);
	}
	

	/**
	 * Returns a named property of a given type or a brand new instance.
	 * */
	public <T> T get(String name, Class<T> type) {
		
		return lookup(name,type).orElseGet(()->create(type));
	}

	public <T> T get(String name, TypeLiteral<T> type) {
		
		return lookup(name,type).orElseGet(()->create(type));
	}

	/**
	 * Returns a named property of a given type or a brand new instance.
	 * */
	@SuppressWarnings("all")
	public <T> T get(String name, T fallback) {
		
		return (T) lookup(name,Object.class).orElse(fallback);
	}

	/**
	 * Changes the anonymous property of a given type, starting from a new instance of the type if the property doesn't exist yet.
	 * */
	public <T> SetClause<T> set(Class<T> type) throws ValidationException {
		
		return set(nameOf(type),type);
	}
	
	/**
	 * Changes the anonymous property of a given type, starting from a new instance of the type if the property doesn't exist yet.
	 * */
	public <T> SetClause<T> set(String name, Class<T> type) throws ValidationException {
		
		return set(name,get(name,type));
	
	}

	public <T> SetClause<T> set(String name, TypeLiteral<T> type) throws ValidationException {
		
		return set(name,get(name,type));
	
	}
	
	/**
	 * Changes the anonymous property of a given type, starting from a given default if the property doesn't exist yet.
	 * */
	public <T> SetClause<T> set(Class<T> type,T defval) throws ValidationException {
		
		return set(nameOf(type),lookup(type).orElse(defval));
	}

	
	/**
	 * Sets a named property to a given serializable value, validating it if required.
	 * */
	public <T> SetClause<T> set(String name, T value) throws ValidationException {
		
		return c -> {

			c.accept(value);
			
			// validates.
			if (value instanceof Validated)
				Validated.class.cast(value).validateNow();
			
			// serialises.
			values.put(name,mapper().valueToTree(value));
			
			return Bag.this;
		};
	}
	
	public void clear(String name) {

		set(name, (Object) null).withDefault();
	}

	public void remove(String name) {

		values.remove(name);
	}
	
	public Bag setAll(Map<String,?> values) throws ValidationException {
		
		values.forEach((name,value) -> set(name,value).withDefault());
			
		return Bag.this;
		
	}

	public Bag copy(Bag bag) throws ValidationException {

		return setAll(bag.values);
		
	}
	
	@Override
	public Iterator<Entry> iterator() {

		var iterator = values.entrySet().iterator();

		return new Iterator<Bag.Entry>() {
      
			
			public boolean hasNext() {
				return iterator.hasNext();
			}
			  
			@SneakyThrows
			public Entry next() {
				var next = iterator.next();
				return new Entry(next.getKey(),next.getValue());
			}
			  
			public void remove() {
				iterator.remove();
			}
		};
	}

	
	
	// helpers
	
	private String nameOf(Class<?> type) {
		return type.getSimpleName().toLowerCase();
	}
	
	// private String nameOf(TypeLiteral<?> type) {
	// 	return type.getType().getTypeName().toLowerCase();
	// }
	

	private <T> T create(Class<T> type) {
		
		try {
			return type.getDeclaredConstructor().newInstance();
		}
		catch(Exception e) {
			throw new RuntimeException("property type has no usable default constructor",e);
		}
	}

	private <T> T create(TypeLiteral<T> type) {
		
		return create(type.getRawType());
	}

	
	//	technical, makes for fluent updates.
	public static interface SetClause<T> {
		
		/**
		 *  A consumer that changes the property.
		 */
		Bag with(Consumer<T> consumer);
		
		default Bag withDefault() {
			return with($->{});
		}
	}
}

