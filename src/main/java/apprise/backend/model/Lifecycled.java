package apprise.backend.model;


public interface Lifecycled<S, SELF extends Lifecycled<S,SELF>> {

	
	Lifecycle<S> lifecycle();
	
	default S state() {
		return lifecycle().state();
	}

	@SuppressWarnings("unchecked")
	default SELF touchedBy(String user) {
		 lifecycle().touchedBy(user);
		 return (SELF) this;
	}

	@SuppressWarnings("unchecked")
	default SELF state(S state) {
		 lifecycle().state(state);
		 return (SELF) this;
	}
	
	default boolean is(S state) {
		return lifecycle().is(state);
	}
	
}
