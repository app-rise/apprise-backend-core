package apprise.backend.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;


/** Some value, possibly in multiple languages.**/


@ToString
@EqualsAndHashCode
@NoArgsConstructor

public class Multilingual implements Comparable<Multilingual> {
	
	/** The default language for {@link Multilingual}s. **/
	
	public final static Language default_language = Language.en;
	
	
	/** Supported languages. **/
	public static enum Language  { en,fr,es,zh,ru,ar }
	
	private final Map<Language,String> values = new HashMap<>();

	public static Multilingual text() {
		return new Multilingual();
	}
	
	public Multilingual(Multilingual other) {
		values.putAll(other.values);
	}
	
	@JsonCreator
	public Multilingual(Map<Language,String> values) {
		this.values.putAll(values);
	}
	
	
	public boolean contains(Language lang) {
		return values.containsKey(lang) && values.get(lang)!=null;
	}
	
	
	/**
	 * Returns this value in a given language, if available. 
	 * Otherwise, it tries to return it in the {@link Multilingual#default_language}.
	 * @return <code>null</code> if all the attempts fail.
	 */
	
	public String in(Language lang) {
		return contains(lang) ? values.get(lang) : values.get(default_language);
	}
	
	
	public String inDefaultLanguage() {
		return in(default_language);
	}
	
	
	/** Set this value in a given language **/
	
	@SuppressWarnings("all")
	public Multilingual in(Language lang, String value) {
		values.put(lang,value);
		return this;
	}
	
	public Multilingual inDefaultLanguage(String value) {
		return in(default_language,value);
	}
	
	
	//natural json serialisation (pairs with @JsonCreator on subclass constructors)
	@JsonValue
	public Map<Language,String> all() {
		return new HashMap<>(values);
	}
	
	/** All languages of this value. */
	
	public Set<Language> languages() {
		return values.keySet();
	}
	
	@Override
	public int compareTo(@NonNull Multilingual o) {
		return this.inDefaultLanguage()==null ? 1 : o.inDefaultLanguage() == null ? -1 : this.inDefaultLanguage().compareTo(o.inDefaultLanguage());
	}
}
