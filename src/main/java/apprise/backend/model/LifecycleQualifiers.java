package apprise.backend.model;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;

import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Qualifier;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *  
 *  {@link Qualifier}s for type-specific lifecycle events.
 *  
 *  
 */

public interface LifecycleQualifiers {

    @Data
    @AllArgsConstructor
    public static class BulkEvent<T> {

        List<String> ids;
    }

    @Target({ FIELD, PARAMETER })
    @Retention(RUNTIME)
    @Documented
    @Qualifier
    @SuppressWarnings("all")
    public @interface Before {

        class Literal extends AnnotationLiteral<Before> implements Before {

        }

        Literal event = new Literal();
    }

    @Target({ FIELD, PARAMETER })
    @Retention(RUNTIME)
    @Documented
    @Qualifier
    @SuppressWarnings("all")
    public @interface Added {

        boolean before() default false;

        class Literal extends AnnotationLiteral<Added> implements Added {

            public boolean before() {
                return false;
            }
        }

        class BeforeLiteral extends AnnotationLiteral<Added> implements Added {

            public boolean before() {
                return true;
            }
        }

        Literal event = new Literal();
        BeforeLiteral beforeEvent = new BeforeLiteral();
    }

    @Target({ FIELD, PARAMETER })
    @Retention(RUNTIME)
    @Documented
    @Qualifier
    @SuppressWarnings("all")
    public @interface Updated {

        boolean before() default false;

        class Literal extends AnnotationLiteral<Updated> implements Updated {

            public boolean before() {
                return false;
            }
        }

        class BeforeLiteral extends AnnotationLiteral<Updated> implements Updated {

            public boolean before() {
                return true;
            }
        }

        Literal event = new Literal();
        BeforeLiteral beforeEvent = new BeforeLiteral();
    }

    @Target({ FIELD, PARAMETER })
    @Retention(RUNTIME)
    @Documented
    @Qualifier
    @SuppressWarnings("all")
    public @interface Removed {

        boolean before() default false;
        boolean cascade() default false;

        class Literal extends AnnotationLiteral<Removed> implements Removed {

            public boolean cascade() {
                return false;
            }

            public boolean before() {
                return false;
            }

        }

        class CascadeLiteral extends AnnotationLiteral<Removed> implements Removed {

            public boolean cascade() {
                return true;
            }

            public boolean before() {
                return false;
            }
        }

        class BeforeLiteral extends AnnotationLiteral<Removed> implements Removed {

            public boolean cascade() {
                return false;
            }

            public boolean before() {
                return true;
            }
        }

        Literal event = new Literal();
        CascadeLiteral cascadeEvent = new CascadeLiteral();
        BeforeLiteral beforeEvent = new BeforeLiteral();
    }

}
