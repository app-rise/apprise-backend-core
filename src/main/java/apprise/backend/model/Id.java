package apprise.backend.model;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.shuffle;
import static java.util.stream.Collectors.joining;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 *  Simplifies and adapts {@see <a href="https://github.com/snimavat/shortid">Snimavat's</a> snimavat's} Java port of  
 * {@see <a href="https://github.com/dylang/shortid">dylang's</a> library. 
 * 
 */
public class Id {

    private static final int version = 1;
    private static final Long REDUCE_TIME = 1459707606518L;

    private static int counter;
    private static int previousSeconds;

    public static String mint(String prefix) {

        String str = "";

        int seconds = (int) Math.floor((System.currentTimeMillis() - REDUCE_TIME) * 0.001);

        if (seconds == previousSeconds)
            counter++;

        else {
            counter = 0;
            previousSeconds = seconds;
        }

        str = str + encode(Alphabet::lookup, version);

        if (counter > 0)
            str = str + encode(Alphabet::lookup, counter);

        str = str + encode(Alphabet::lookup, seconds);

        return format("%s-%s",prefix,str);
    }

    static class Alphabet {

        static long seed = 1;
        private static final Random random = new Random(seed);

        static String alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$~";
        static Optional<String> shuffled = Optional.empty();

        static Character lookup(int index) {

            return shuffled.orElseGet(() -> {

                List<String> chars = asList(alphabet.split(""));
               
                shuffle(chars, random);
                
                String shuffledAlphabet = chars.stream().collect(joining(""));

                shuffled = Optional.of(shuffledAlphabet);

                return shuffled.get();

            }).charAt(index);
        }

    }

    static String encode(Function<Integer, Character> lookup, int number) {


        Supplier<Integer> randomByte =  () ->((int) Math.floor(Math.random() * 256)) & 0x30;

        int loopCounter = 0;
        boolean done = false;

        String str = "";

        while (!done) {
            str = str + lookup.apply(((number >> (4 * loopCounter)) & 0x0f) | randomByte.get());
            done = number < (Math.pow(16, loopCounter + 1));
            loopCounter++;
        }

        return str;
    }


}
